/* eslint no-alert: 0, jsx-a11y/accessible-emoji: 0  */

import React, { Component } from 'react';
import { View, StyleSheet,Text, Linking,TextInput, Image, TouchableOpacity, Dimensions } from 'react-native';

import { GiftedChat, Send } from 'react-native-gifted-chat';

import messagesData from './data';
import NavBar from './NavBar';
import CustomView from './CustomView';
import InputToolbar from './components/InputToolbar'
import MessageContainer from './components/MessageContainer'
import CustomBubble from './components/CustomBubble'
import MessageVideo from './components/MessageVideo'
const ImagePicker = require('react-native-image-picker');

const styles = StyleSheet.create({
  container: { flex: 1 },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    position:'absolute',
    height: Dimensions.get('window').height,
    width: Dimensions.get('window').width
 },
 capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    color: '#000',
    padding: 10,
    margin: 40
 },
});

const filterBotMessages = (message) => !message.system && message.user && message.user._id && message.user._id === 2;
const findStep = (step) => (_, index) => index === step - 1;

export default class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      messages: [],
      step: 0,
      text:'test',
      avatarSource:{},
    };

    this.onSend = this.onSend.bind(this);
    this.parsePatterns = this.parsePatterns.bind(this);
    this.renderInputToolbar = this.renderInputToolbar.bind(this)
    this._TakingPhoto = this._TakingPhoto.bind(this)
    this.onSendPhoto = this.onSendPhoto.bind(this)
    this._TakingVideo = this._TakingVideo.bind(this)
    this.renderBubble = this.renderBubble.bind(this)
    this.renderMessageImage = this.renderMessageImage.bind(this)
    this.onSendVideo = this.onSendVideo.bind(this)
  }

  componentWillMount() {
    // init with only system messages
    this.setState({ messages: messagesData.filter((message) => message.system) });
  }
  componentWillUpdate(nextProps, nextState) {
      if(nextState.avatarSource.timestamp != this.state.avatarSource.timestamp){
        // this.onSendPhoto(nextState.avatarSource.source.data)
        if(nextState.avatarSource.source.isVideo){
          this.onSendVideo(nextState.avatarSource.source.uri)
        } else {
          this.onSendPhoto(nextState.avatarSource.source.uri)

        }
      }
  }

  onSend(messages = []) {
    const step = this.state.step + 1;
    this.setState((previousState) => ({
      messages: GiftedChat.append(previousState.messages, [{ ...messages[0], sent: true, received: true }]),
      step,
    }));
    setTimeout(() => this.botSend(step), 1500 + Math.round(Math.random() * 1000));
  }

  onSendPhoto(photoURI) {
    const step = this.state.step + 1;
    const msg = {
      _id: Math.round(Math.random() * 1000000),
      text: 'This is my photo',
      createdAt: new Date(),
      user: {
        _id: 1,
        name: 'James',
      },
      // image: 'data:image/jpg;base64,'+photoURI,
      image: photoURI,
      sent: true,
      received: true,
    }

    this.setState((preState)=>({
      messages:GiftedChat.append(preState.messages, [msg]), step
    }))
  }

  onSendVideo(photoURI) {
    const step = this.state.step + 1;
    const msg = {
      _id: Math.round(Math.random() * 1000000),
      text: 'This is my Video',
      createdAt: new Date(),
      user: {
        _id: 1,
        name: 'James',
      },
      // image: 'data:image/jpg;base64,'+photoURI,
      image: photoURI,
      isVideo:true,
      sent: true,
      received: true,
    }
    this.setState((preState)=>({
      messages:GiftedChat.append(preState.messages, [msg]), step
    }))
  }

  botSend(step = 0) {
    const newMessage = messagesData
      .reverse()
      .filter(filterBotMessages)
      .find(findStep(step));
    if (newMessage) {
      this.setState((previousState) => ({
        messages: GiftedChat.append(previousState.messages, newMessage),
      }));
    }
  }

parsePatterns(linkStyle) {
  return [
    {
      pattern: /#(\w+)/,
      style: { ...linkStyle, color: 'orange' },
      onPress: () => Linking.openURL('http://gifted.chat'),
    },
  ];
}

renderInputToolbar(props){
  return (
    <InputToolbar {...props}/>
  )
}
renderBubble(props) {
  return (
    <CustomBubble {...props} />
  )
}

renderMessageImage(props) {
  return (
    <MessageVideo {...props} /> 
  )
}

  _TakingPhoto(){
 
    const options = {
      title: 'Select a Photo',
      storageOptions: {
        skipBackup: true,
        path: 'images'
      }
    };
    
    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);
      if (response.didCancel) {
        console.log('User cancelled image picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        // let source = { uri: response.uri, data:response.data };
        let source = { uri: response.uri };
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };
        this.setState({
          avatarSource: {
            timestamp: Date.now(), source}
        });
      }
    });
  }
  _TakingVideo(){
    const options = {
      title: 'Select a Video',
      takePhotoButtonTitle:'Take a Video',
      storageOptions: {
        skipBackup: true,
        path: 'images'
      },
      chooseFromLibraryButtonTitle:null, //prevent users to upload their own large videos
      mediaType:'video',
      videoQuality:'medium',
      durationLimit:10,
      noData:true
    };
    
    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);
      if (response.didCancel) {
        console.log('User cancelled image picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response.uri, isVideo:true};
        this.setState({
          avatarSource: {
            timestamp: Date.now(), source}
        });
      }
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <NavBar />
        <GiftedChat
          messages={this.state.messages}
          onSend={this.onSend}
          renderCustomView={CustomView}
          user={{
            _id: 1,
            name:'John Philip'
          }}
          renderBubble={this.renderBubble}
          renderMessageImage={this.renderMessageImage}
          parsePatterns={this.parsePatterns}
          renderInputToolbar={this.renderInputToolbar} 
          bottomOffset={50}
          listViewProps={{style:{marginBottom:50}}}
        />
          <View style={{ position:'absolute',
              height: 50,
              bottom: 0,marginLeft:Dimensions.get('window').width/2-32,marginBottom:10,
            }}>
            <TouchableOpacity onPress={this._TakingPhoto} onLongPress={this._TakingVideo} activeOpacity={0.7}>
              <Image
                source={require('./assets/camera-icon.png')}
                style={{width:50, height:50,marginTop:10}}
              />
            </TouchableOpacity>
          </View>
      </View>
    );
  }

}
