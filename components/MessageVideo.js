import PropTypes from 'prop-types';
import React from 'react';
import {
  StyleSheet,
  View,
  ViewPropTypes,
  TouchableOpacity,
  Image,
  Text,
  Dimensions
} from 'react-native';

import Video from 'react-native-video'
import Lightbox from 'react-native-lightbox';
export default class MessageVideo extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      stop:true,
    }
    this.playVideo = this.playVideo.bind(this)
    this.stopVideo = this.stopVideo.bind(this)
    
  }
  componentDidMount() {
    this.player.seek(1)
  }
  playVideo() {
    this.player.seek(0)
    this.setState({stop:false})
    this.player.presentFullscreenPlayer()
  }
  stopVideo() {
    this.setState({stop:true})
   //this.player.dismissFullscreenPlayer()
  }
  render() {
    return(
        <View style={[styles.container, this.props.containerStyle]}>
        
            {/* <Lightbox
                activeProps={{
                  style: styles.imageActive,
                }}
                // {...this.props.lightboxProps}
                onOpen={this.playVideo}
                onClose={this.stopVideo}
                swipeToDismiss={false}
              > */}
                <Video source={{uri:this.props.currentMessage.image}}   // Can be a URL or a local file. 
                    ref={(ref) => {
                      this.player = ref
                    }}                                     
                    rate={1.0}                              // 0 is paused, 1 is normal. 
                    volume={1.0}                            // 0 is muted, 1 is normal. 
                    muted={false}                           // Mutes the audio entirely. 
                    paused={!!this.state.stop}                          // Pauses playback entirely. 
                    repeat={false}                           // Repeat forever. 
                    playInBackground={false}                // Audio continues to play when app entering background. 
                    playWhenInactive={false}                // [iOS] Video continues to play when control or notification center are shown. 
                    onEnd={this.stopVideo} 
                    ignoreSilentSwitch={"ignore"}          
                    progressUpdateInterval={250.0}          
                    style={styles.image}
                  />
            {/* </Lightbox> */}
            {this.state.stop ?
            <TouchableOpacity onPress={this.playVideo}>
              <Image 
                  source={require('../assets/play-icon.png')}
                  style={{width:50, height:50, position:'absolute',top:-70,left:50,zIndex:111}}
              />
            </TouchableOpacity>
            :
            <TouchableOpacity onPress={this.stopVideo}>
              <Image 
                  source={require('../assets/stop-icon.png')}
                  style={{width:50, height:50, position:'absolute',top:-70,left:50,zIndex:111}}
              />
            </TouchableOpacity>
            }
        </View>
      )
  }
}

const styles = StyleSheet.create({
  container: {
  },
  image: {
    width: 150,
    height: 100,
    borderRadius: 13,
    margin: 3,
    resizeMode: 'cover',
  },
  imageActive: {
    flex: 1,
    resizeMode: 'contain',
  }
});

MessageVideo.defaultProps = {
  currentMessage: {
    image: null,
  },
  containerStyle: {},
  imageStyle: {},
};

MessageVideo.propTypes = {
  currentMessage: PropTypes.object,
  containerStyle: ViewPropTypes.style,
  imageStyle: Image.propTypes.style,
  imageProps: PropTypes.object,
  lightboxProps: PropTypes.object,
};
